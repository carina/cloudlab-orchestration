Collection Script for CloudLab Benchmarks
===========

This repository contains the script that we execute in order to 
orchestrate, and collect results from, benchmarks on CloudLab machines.

The structure of the repository is the following:

- `orchestration.py`. The script containing logic to allocate nodes, deploy
  software, run tests and retrieve results.
